package net.ddns.victornilsson.activityrecognition;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.ActionMenuItemView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.ClipData;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toolbar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import static android.os.SystemClock.elapsedRealtimeNanos;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    SensorManager sensorManager;
    Sensor accSensor;
    Sensor gyroSensor;
    Sensor gravitySensor;

    private int mTimerUpdate = 500;
    private final int mAllowSaveCycles = 5;
    private final int mStopSaveCycles = 2;
    private boolean mAllowSave = true;
    private int mCycle = 0;

    ProgressBar cyclebar;
    ProgressBar runningbar;
    ProgressBar walkingbar;
    ProgressBar speedwalkingbar;

    Context mContext;

    SimpleDateFormat sdf = new SimpleDateFormat("MM:dd HH:mm:ss", Locale.getDefault());


    String url = "http://victornilsson.ddns.net:5005/predict";
    String url_health = "http://victornilsson.ddns.net:5005/health";

    //String url = "http://192.168.1.72:5000/predict";
    //String url_health = "http://192.168.1.72:5000/health";

    private final int fillColor = Color.argb(0 , 0,0, 0);

    private LineChart chart;
    private LineChart chartAuto;

    private LineDataSet dataCycling, dataRunning, dataWalking, dataSpeedWalking;
    private LineDataSet dataAuto;
    private static final int REQUEST_WAKE_LOCK = 0;
    private PowerManager.WakeLock mWakeLock;

    private int nCycling = 0;
    private int nWalking = 0;
    private int nSpeedWalking = 0;
    private int nRunning = 0;
    private int nTotal = 0;


    Timer sensorTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        // https://stackoverflow.com/questions/17400940/is-it-possible-to-detect-motion-when-screen-is-off
        //int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WAKE_LOCK);

        //if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
        //    ActivityCompat.requestPermissions(this,
         //           new String[]{Manifest.permission.WAKE_LOCK},
        //            REQUEST_WAKE_LOCK);
        //} else {
        //    acquireWakeLock();
        //}


        cyclebar = findViewById(R.id.progressBarCycling);
        runningbar = findViewById(R.id.progressBarRunning);
        walkingbar = findViewById(R.id.progressBarWalking);
        speedwalkingbar = findViewById(R.id.progressBarSpeedWalking);

        setupChart();

        mContext = this.getApplicationContext();

        sensorManager = (SensorManager)this.getSystemService(Context.SENSOR_SERVICE);
        accSensor = sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);
        gyroSensor = sensorManager.getSensorList(Sensor.TYPE_GYROSCOPE).get(0);

        gravitySensor = sensorManager.getSensorList(Sensor.TYPE_GRAVITY).get(0);

        String acc = accSensor.getName();
        String gyro = accSensor.getName();
        String gravity = gravitySensor.getName();
        startRunning();

        startTimer();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    private void startTimer(){
        sensorTimer = new Timer();
        sensorTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                int compareCycle;
                if (mAllowSave){
                    compareCycle = mAllowSaveCycles;

                }
                else {
                    compareCycle = mStopSaveCycles;
                }
                if (mCycle == compareCycle){
                    mAllowSave = !mAllowSave;
                    mCycle=0;
                    if (mAllowSave){
                        startRunning();
                        JsonObjectRequest isServerUprequest = createServerHealthRequest();
                        BackendCommunicator.getInstance(mContext).addToRequestQueue(isServerUprequest);

                    } else {
                        stopRunning();
                        System.out.println("length of sensor: acc "+ accSensorEvents.size());
                        System.out.println("length of sensor: gyro "+ gyroSensorEvents.size());
                        System.out.println("length of sensor: orient "+ gravitySensorEvents.size());
                        try {
                            testPostRequest.add(accSensorEvents,
                                    gyroSensorEvents, gravitySensorEvents);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        JsonObjectRequest request = createREquest(testPostRequest.object);
                        if (is_server_up) {
                            String currentDateandTime = sdf.format(new Date());
                            Log.i(TAG, "sending: " + currentDateandTime);
                            BackendCommunicator.getInstance(mContext).addToRequestQueue(request);
                        } else {
                            String currentDateandTime = sdf.format(new Date());
                            Log.i(TAG, "Im not sending as the server is not up: " + currentDateandTime);
                        }
                        accSensorEvents = new ArrayList<>();
                        gravitySensorEvents = new ArrayList<>();
                        gyroSensorEvents = new ArrayList<>();
                    }
                }
                mCycle++;

            }
        }, mTimerUpdate,mTimerUpdate);
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopRunning();
        sensorTimer.cancel();
    }

    private void acquireWakeLock(){
        //PowerManager pm = (PowerManager) getSystemService(getApplicationContext().POWER_SERVICE);
        //mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "My Tag:");

        //mWakeLock.acquire();
    }

    public void startRunning(){
        sensorManager.registerListener(mAccSensorListener, accSensor, SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(mGyroSensorListener, gyroSensor, SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(mGravitySensorListener, gravitySensor, SensorManager.SENSOR_DELAY_FASTEST);
    }

    public void stopRunning(){
        sensorManager.unregisterListener(mAccSensorListener);
        sensorManager.unregisterListener(mGyroSensorListener);
        sensorManager.unregisterListener(mGravitySensorListener);
    }

    List<SensorEventTime> accSensorEvents = new ArrayList<SensorEventTime>();
    List<SensorEventTime> gyroSensorEvents = new ArrayList<SensorEventTime>();
    List<SensorEventTime> gravitySensorEvents = new ArrayList<SensorEventTime>();


    SensorEventListener mAccSensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            SensorEventTime newEvent = new SensorEventTime();
            newEvent.event = event;
            newEvent.timestamp = getSensorTimestamp(event);
            accSensorEvents.add(newEvent);
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };

    SensorEventListener mGyroSensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            SensorEventTime newEvent = new SensorEventTime();
            newEvent.event = event;
            newEvent.timestamp = getSensorTimestamp(event);
            gyroSensorEvents.add(newEvent);
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };

    SensorEventListener mGravitySensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            SensorEventTime newEvent = new SensorEventTime();
            newEvent.event = event;
            newEvent.timestamp = getSensorTimestamp(event);
            gravitySensorEvents.add(newEvent);
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };


    private long getSensorTimestamp(SensorEvent event){
        // the latest sensor event timestamp is calculated in nanoseconds of uptime and not actual real time
        // so we convert it to a unix timeformat
        long SensorEventDiff =(event.timestamp - elapsedRealtimeNanos() ) / 1000000L;
        long timestamp = (new Date()).getTime()
                + SensorEventDiff;
        return timestamp;
    }

    private boolean is_server_up = false;

    private JsonObjectRequest createServerHealthRequest(){
        return new JsonObjectRequest(Request.Method.POST, url_health, null,
                new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ActionMenuItemView item = findViewById(R.id.server_status);

                try{
                    String health = (String)response.get("health");
                    if (health.equals("ok")){
                        item.setIcon(getResources().getDrawable(R.drawable.ic_server_up));
                        is_server_up= true;
                    } else {
                        item.setIcon(getResources().getDrawable(R.drawable.ic_server_down));
                        is_server_up = false;
                    }
                }
                 catch (Exception e){
                     item.setIcon(getResources().getDrawable(R.drawable.ic_server_down));
                     is_server_up = false;
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ActionMenuItemView item = findViewById(R.id.server_status);
                item.setIcon(getResources().getDrawable(R.drawable.ic_server_down));

                is_server_up = false;
            }
        });
    }

    private int getMax(int cycle, int running, int speed, int walking){
        int max = cycle;
        int maxIndex = 0;
        if (running > max){
            max = running;
            maxIndex = 1;
        }
        if (speed > max){
            max = speed;
            maxIndex = 2;
        }
        if (walking > max){
            max = walking;
            maxIndex = 3;
        }


        return maxIndex;
    }

    private JsonObjectRequest createREquest(JSONObject request){

        return new JsonObjectRequest(Request.Method.POST,
                url, request, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(JSONObject response) {
                try {

                    String currentDateandTime = sdf.format(new Date());
                    Log.i(TAG, "retrieving: " + currentDateandTime);
                    int cycle = (int)(100 * (double)((JSONArray)(response.get("pred"))).get(0));
                    int running = (int)(100 * (double)((JSONArray)(response.get("pred"))).get(1));
                    int speed = (int)(100 * (double)((JSONArray)(response.get("pred"))).get(2));
                    int walking = (int)(100 * (double)((JSONArray)(response.get("pred"))).get(3));
                    float val = ((Double)((response.get("autoencoder")))).floatValue();
                    int maxIndex = getMax(cycle, running, speed, walking);
                    switch (maxIndex){
                        case 0:
                            nCycling++;
                            nTotal++;
                            break;
                        case 1:
                            nRunning++;
                            nTotal++;
                            break;
                        case 2:
                            nSpeedWalking++;
                            nTotal++;
                            break;
                        case 3:
                            nWalking++;
                            nTotal++;
                            break;
                        default:
                            break;
                    }
                    cyclebar.setProgress((int)(((float)nCycling/nTotal) * 100));
                    runningbar.setProgress((int)(((float)nRunning/nTotal) * 100));
                    walkingbar.setProgress((int)(((float)nWalking/nTotal) * 100));
                    speedwalkingbar.setProgress((int)(((float)nSpeedWalking/nTotal) * 100));

                    runningbar.setMax(100);
                    cyclebar.setMax(100);
                    walkingbar.setMax(100);
                    speedwalkingbar.setMax(100);

                    System.out.println("Setting new entry with autoencoder: " + val);
                    System.out.println("Setting new cycle: " + cycle);
                    System.out.println("Setting new running: " + running);
                    System.out.println("Setting new spedd: " + speed);
                    System.out.println("Setting new walking: " + walking);
                    dataCycling.addEntry(new Entry(nextIdx,cycle/100.0f ));
                    dataRunning.addEntry(new Entry(nextIdx,running/100.0f ));
                    dataSpeedWalking.addEntry(new Entry(nextIdx,speed/100.0f ));
                    dataWalking.addEntry(new Entry(nextIdx,walking/100.0f ));
                    dataAuto.addEntry(new Entry(nextIdx,val ));
                    nextIdx++;
                    chart.getData().notifyDataChanged();
                    chart.notifyDataSetChanged();
                    chart.invalidate();
                    chartAuto.getData().notifyDataChanged();
                    chartAuto.notifyDataSetChanged();
                    chartAuto.invalidate();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
    }


    private void setupChart(){
        chart = findViewById(R.id.chart);
        chartAuto = findViewById(R.id.chart2);

        chart.setBackgroundColor(Color.WHITE);
        chartAuto.setBackgroundColor(Color.WHITE);
        chart.setGridBackgroundColor(fillColor);
        chartAuto.setGridBackgroundColor(fillColor);
        chart.setDrawGridBackground(false);
        chartAuto.setDrawGridBackground(false);

        chart.setDrawBorders(false);
        chartAuto.setDrawBorders(false);

        // no description text
        chart.getDescription().setEnabled(false);
        chartAuto.getDescription().setEnabled(false);

        // if disabled, scaling can be done on x- and y-axis separately
        chart.setPinchZoom(false);
        chartAuto.setPinchZoom(false);

        XAxis xAxis = chart.getXAxis();
        XAxis xAxis2 = chartAuto.getXAxis();
        xAxis.setEnabled(false);
        xAxis2.setEnabled(false);

        YAxis leftAxis = chart.getAxisLeft();
        YAxis leftAxis2 = chartAuto.getAxisLeft();
        leftAxis.setAxisMaximum(1.01f);
        leftAxis.setAxisMinimum(0f);
        leftAxis.setDrawAxisLine(false);
        leftAxis.setDrawZeroLine(false);
        leftAxis.setDrawGridLines(false);
        leftAxis.setDrawGridLinesBehindData(false);
        leftAxis.setDrawLimitLinesBehindData(false);

        leftAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getAxisLabel(float value, AxisBase axis) {
                return "" + (int)(value*100)+ "%";
            }
        });

        leftAxis2.setAxisMaximum(0.10f);
        leftAxis2.setAxisMinimum(0f);
        leftAxis2.setDrawAxisLine(false);
        leftAxis2.setDrawZeroLine(false);
        leftAxis2.setDrawGridLines(false);
        leftAxis2.setDrawGridLinesBehindData(false);
        leftAxis2.setDrawLimitLinesBehindData(false);



        YAxis rightAxis = chart.getAxisRight();
        YAxis rightAxis2 = chartAuto.getAxisRight();
        rightAxis.setAxisMaximum(1.01f);
        rightAxis2.setAxisMaximum(0.05f);
        rightAxis2.setAxisMinimum(0f);
        rightAxis.setAxisMinimum(0f);
        rightAxis.setDrawAxisLine(false);
        rightAxis2.setDrawAxisLine(false);
        //rightAxis.setDrawZeroLine(false);
        //rightAxis.setDrawGridLines(false);
        //rightAxis.setDrawGridLinesBehindData(false);
        //rightAxis.setDrawLimitLinesBehindData(false);
        rightAxis.setDrawLabels(false);
        rightAxis2.setDrawLabels(false);


        // add data
        setData();
    }
    private static int nextIdx = 0;
    private void setData() {

        ArrayList<Entry> values1 = new ArrayList<>();

        ArrayList<Entry> values2 = new ArrayList<>();
        ArrayList<Entry> values3 = new ArrayList<>();
        ArrayList<Entry> values4 = new ArrayList<>();

        ArrayList<Entry> valueAuto = new ArrayList<>();


        if (chartAuto.getData() != null &&
                chartAuto.getData().getDataSetCount() > 0){
            dataAuto = (LineDataSet) chartAuto.getData().getDataSetByIndex(0);

            dataAuto.setValues(valueAuto);
            chartAuto.getData().notifyDataChanged();
            chartAuto.notifyDataSetChanged();
        } else {
            dataAuto = new LineDataSet(valueAuto, "Dissimilarity");

            dataAuto.setAxisDependency(YAxis.AxisDependency.LEFT);

            dataAuto.setColor(getColor(R.color.auto));
            dataAuto.setDrawCircles(false);
            dataAuto.setLineWidth(2f);
            dataAuto.setCircleRadius(3f);
            dataAuto.setFillAlpha(50);
            dataAuto.setDrawFilled(true);
            dataAuto.setFillColor(getColor(R.color.auto));
            dataAuto.setHighLightColor(getColor(R.color.auto));
            dataAuto.setDrawCircleHole(false);
            dataAuto.setFillFormatter(new IFillFormatter() {
                @Override
                public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {

                    return chart.getAxisLeft().getAxisMinimum();
                }
            });


            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
            dataSets.add(dataAuto);



            // create a data object with the data sets
            LineData data = new LineData(dataSets);
            data.setDrawValues(false);

            // set data
            chartAuto.setData(data);
        }

        if (chart.getData() != null &&
                chart.getData().getDataSetCount() > 0) {
            dataCycling = (LineDataSet) chart.getData().getDataSetByIndex(0);
            dataRunning = (LineDataSet) chart.getData().getDataSetByIndex(1);
            dataWalking = (LineDataSet) chart.getData().getDataSetByIndex(2);
            dataSpeedWalking = (LineDataSet) chart.getData().getDataSetByIndex(3);


            dataCycling.setValues(values1);
            dataRunning.setValues(values2);
            dataWalking.setValues(values3);
            dataSpeedWalking.setValues(values4);

            chart.getData().notifyDataChanged();
            chart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            dataCycling = new LineDataSet(values1, "Cycle");
            dataRunning = new LineDataSet(values2, "Run");
            dataWalking = new LineDataSet(values3, "Walk");
            dataSpeedWalking = new LineDataSet(values4, "Speed walk");

            dataCycling.setAxisDependency(YAxis.AxisDependency.LEFT);

            dataCycling.setColor(getColor(R.color.cycle));
            dataCycling.setDrawCircles(false);
            dataCycling.setLineWidth(2f);
            dataCycling.setCircleRadius(3f);
            dataCycling.setFillAlpha(0);
            dataCycling.setDrawFilled(false);
            dataCycling.setFillColor(getColor(R.color.cycle));
            dataCycling.setHighLightColor(getColor(R.color.cycle));
            dataCycling.setDrawCircleHole(false);
            dataCycling.setFillFormatter(new IFillFormatter() {
                @Override
                public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {

                    return chart.getAxisLeft().getAxisMinimum();
                }
            });

            dataRunning.setAxisDependency(YAxis.AxisDependency.LEFT);
            dataRunning.setColor(getColor(R.color.running));
            dataRunning.setDrawCircles(false);
            dataRunning.setLineWidth(2f);
            dataRunning.setCircleRadius(3f);
            dataRunning.setFillAlpha(0);
            dataRunning.setDrawFilled(false);
            dataRunning.setFillColor(getColor(R.color.running));
            dataRunning.setHighLightColor(getColor(R.color.running));
            dataRunning.setDrawCircleHole(false);
            dataRunning.setFillFormatter(new IFillFormatter() {
                @Override
                public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {

                    return chart.getAxisLeft().getAxisMinimum();
                }
            });

            dataWalking.setAxisDependency(YAxis.AxisDependency.LEFT);


            dataWalking.setColor(getColor(R.color.walking));
            dataWalking.setDrawCircles(false);
            dataWalking.setLineWidth(2f);
            dataWalking.setCircleRadius(3f);
            dataWalking.setFillAlpha(0);
            dataWalking.setDrawFilled(false);
            dataWalking.setFillColor(getColor(R.color.walking));
            dataWalking.setHighLightColor(getColor(R.color.walking));
            dataWalking.setDrawCircleHole(false);
            dataWalking.setFillFormatter(new IFillFormatter() {
                @Override
                public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {

                    return chart.getAxisLeft().getAxisMinimum();
                }
            });

            dataSpeedWalking.setAxisDependency(YAxis.AxisDependency.LEFT);

            dataSpeedWalking.setColor(getColor(R.color.speed_walking));
            dataSpeedWalking.setDrawCircles(false);
            dataSpeedWalking.setLineWidth(2f);
            dataSpeedWalking.setCircleRadius(3f);
            dataSpeedWalking.setFillAlpha(0);
            dataSpeedWalking.setDrawFilled(false);
            dataSpeedWalking.setFillColor(getColor(R.color.speed_walking));
            dataSpeedWalking.setHighLightColor(getColor(R.color.speed_walking));
            dataSpeedWalking.setDrawCircleHole(false);

            dataSpeedWalking.setFillFormatter(new IFillFormatter() {
                @Override
                public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {

                    return chart.getAxisLeft().getAxisMinimum();
                }
            });

            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
            dataSets.add(dataCycling);
            dataSets.add(dataRunning);
            dataSets.add(dataWalking);
            dataSets.add(dataSpeedWalking);



            // create a data object with the data sets
            LineData data = new LineData(dataSets);
            data.setDrawValues(false);

            // set data
            chart.setData(data);

        }
    }

}