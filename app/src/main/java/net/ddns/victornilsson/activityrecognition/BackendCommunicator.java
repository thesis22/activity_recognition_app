package net.ddns.victornilsson.activityrecognition;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

public class BackendCommunicator {

        // use the following link to get knoeledge about the
    // JsonObjectRequest https://developer.android.com/training/volley/index.html

    private static BackendCommunicator sInstance;
    private RequestQueue mRequestQueue;
    private static Context sContext;

    private BackendCommunicator(Context context){
        sContext = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized  BackendCommunicator getInstance(Context context){
        if (sInstance == null){
            sInstance = new BackendCommunicator(context);
        }
        return sInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null){
            mRequestQueue = Volley.newRequestQueue(sContext.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> request){
        getRequestQueue().add(request);
    }
}
